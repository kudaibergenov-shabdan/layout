import './App.css';
import {BrowserRouter, Switch, Route} from "react-router-dom";
import Layout from "./components/UI/Layout/Layout";
import Page from "./container/Page/Page";
import Admin from "./container/Admin/Admin";

function App() {
    return (
        <BrowserRouter>
            <Layout>
                <Switch>
                    <Route path="/" exact component={Page}/>
                    <Route path="/home" exact component={Page}/>
                    <Route path="/about" exact component={Page}/>
                    <Route path="/contacts" exact component={Page}/>
                    <Route path="/divisions" exact component={Page}/>
                    <Route path="/admin" component={Admin}/>
                    <Route render={() => <h1>Not found</h1>} />
                </Switch>
            </Layout>
        </BrowserRouter>
    );
}

export default App;
