import axios from "axios";

const axiosApi = axios.create({
    baseURL: 'https://my-blog-fc8f8-default-rtdb.firebaseio.com'
});

export default axiosApi;