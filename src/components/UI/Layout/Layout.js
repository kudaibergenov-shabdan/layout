import React from 'react';
import './Layout.css';
import NavigationItems from "../Navigation/NavigationItems/NavigationItems";

const Layout = ({children}) => {
    return (
        <>
            <header className="header">
                <h3 className="header-title">Static pages</h3>
                <nav className="main-nav">
                    <NavigationItems />
                </nav>
            </header>
            <main>
                {children}
            </main>
        </>
    );
};

export default Layout;