import React from 'react';
import {NavLink} from "react-router-dom";
import './NavigationItem.css';

const NavigationItem = ({to, exact, children}) => {
    return (
        <li className="main-nav__list-item">
            <NavLink to={to} exact={exact} className="main-nav__list-item-link">{children}</NavLink>
        </li>
    );
};

export default NavigationItem;