import React from 'react';
import './NavigationItems.css';
import NavigationItem from "./NavigationItem/NavigationItem";

const NavigationItems = () => {
    return (
        <ul className="main-nav__list">
            <NavigationItem to="/" exact>Home</NavigationItem>
            <NavigationItem to="/about" >About</NavigationItem>
            <NavigationItem to="/contacts" >Contacts</NavigationItem>
            <NavigationItem to="/divisions" >Divisions</NavigationItem>
            <NavigationItem to="/admin" >Admin</NavigationItem>
        </ul>
    );
};

export default NavigationItems;