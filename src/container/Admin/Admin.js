import React, {useEffect, useState} from 'react';
import './Admin.css';
import axiosApi from "../../axiosApi";

const Admin = ({history}) => {
    const defaultPage = {
        title: '',
        content: '',
        pageName: ''
    };
    const [page, setPage] = useState(defaultPage);
    const [pages, setPages] = useState([page]);

    useEffect(() => {
        const fetchPages = async () => {
            const response = await axiosApi('/pages.json');
            setPages(prev => (
                prev.concat(Object.keys(response.data).map(page => {
                    return {...response.data[page], pageName: page};
                }))
            ));
        };
        fetchPages().catch(err => console.error(err));
    }, []);

    const onInputChange = e => {
        const {name, value} = e.target;
        setPage(prevPage => {
                return {
                    ...prevPage,
                    [name]: value
                }
            }
        );
    };

    const onSelectChange = async e => {
        const page = e.target.value;
        try {
            if (page) {
                const response = await axiosApi.get(`/pages/${page}.json`);
                setPage(() => {
                    const newPage = {...response.data, pageName: page};
                    if (!newPage.content) {
                        newPage.content = '';
                    }
                    return newPage;
                });
            } else {
                setPage(defaultPage);
                throw new Error("page hasn't been choosen");
            }

        } catch (error) {
            console.error(error);
        }
    }

    const onSubmitForm = async e => {
        e.preventDefault();
        try {
            if (page.pageName && page.title) {
                await axiosApi.put(`/pages/${page.pageName}.json`, {
                        title: page.title,
                        content: page.content
                    }
                );
            } else {
                throw new Error();
            }
        } catch (e) {
            history.push('/home');
        } finally {
            history.push(`/${page.pageName}`);
        }
    }

    return (
        <div className="Admin">
            <h3>Edit Pages</h3>
            <form onSubmit={onSubmitForm}>
                <div className="row">
                    <p className="title">Select page</p>
                    <select className="field" onChange={onSelectChange}>
                        {pages.map(page => (
                            <option key={page.pageName} value={page.pageName}>
                                {(page.title) ? page.title : 'Choose from list...'}
                            </option>
                        ))}
                    </select>
                </div>
                <div className="row">
                    <p className="title">Title</p>
                    <input
                        className="field"
                        type="text"
                        name="title"
                        value={page.title}
                        onChange={e => onInputChange(e)}
                    />
                </div>
                <div className="row">
                    <p className="title">Content</p>
                    <textarea
                        className="field text-content"
                        name="content"
                        value={page.content}
                        onChange={e => onInputChange(e)}
                    />
                </div>
                <div className="btn-block">
                    <button className="btn-save">Save</button>
                </div>
            </form>
        </div>
    );
};

export default Admin;