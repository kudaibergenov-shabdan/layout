import React, {useCallback, useEffect, useState} from 'react';
import axiosApi from "../../axiosApi";
import './Page.css';

const Page = ({match}) => {
    const [content, setContent] = useState(null);

    const fetchData = useCallback(async () => {
        const path = match.path;
        const pageRoute = (path === '/') ? 'home' : path.substring(1);
        const response = await axiosApi.get(`/pages/${pageRoute}.json`);
        const data = response.data;
        if (!data.content) {
            throw new Error('No data info');
        } else {
            return data;
        }
    }, [match.path]);

    useEffect(() => {
        fetchData()
            .then(content => setContent(content))
            .catch(err => setContent({content: err.toString()}));
    }, [fetchData]);

    return (
        <div className="Page">
            {
                content &&
                <div className="content">
                    <h3>{content.title}</h3>
                    <p>{content.content}</p>
                </div>
            }
        </div>
    );
};

export default Page;